# Dockerfile

![statefull](screenshots/dockerfile.png)

Dockerizace IoT Platfomy, kterou si vyvýjím ve volném čase.

# Docker prohlížení vrstev

![statefull](screenshots/dive.png)

Inspekce vytvořeného image pomocí nástroje `dive`. Označené první 4 vrstvy jsou z podkladového obrazu `node:16-apline3.11`. Zbylé jsou vytvořeny v rámci Dockerfile při sestavování.

![statefull](screenshots/history.png)

Pohled na image pomocí `docker history`.

![statefull](screenshots/browse_layer.png)

Zobrazení jedné vrstvy z exportu image - nalezení dle ID vrstvy, rozbalení layer.tar a nalezení souboru, který byl vrámci dané vrstvy tyvořen.

# Container

Instaloval jsem podle tohoto [návodu](https://docs.fedoraproject.org/en-US/fedora-server/container-nspawn-install/) - zjistil jsem, že heslo zle změnit přímo pomocí `systemd-nspawn` -> není potřeba editovat passwd ručně

![statefull](screenshots/fedora_container_boot.png)

Spuštění a boot po instalaci Fedora 35 do kontaineru.

![statefull](screenshots/fedora_container.png)

Přihlášení v kontejneru a zobrazení verze OS.

# Systemd unit

Využil jsem Unit file napsaný pro spuštění mnou vyvíjené IoT Platformy.

![statefull](screenshots/original_unit.png)

![statefull](screenshots/unsafe_report.png)

Původní unit file a security report.

![statefull](screenshots/safe_unit.png)

![statefull](screenshots/medium_report.png)

Finální unit file a report po zavedení bezpečnostních features.
