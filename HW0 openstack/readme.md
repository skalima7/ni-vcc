# HW 01

Funkční nainstalovaný OpenStack s dvěma výpočetněními uzly (hypervizory, master je hlavní VM, slave je druhá menší VM). Lze vytvářet instance, které obdrží IP adresu z DHCP serveru. Instance lze spouštět na obou výpočetních uzlech, funguje mezi nimi ping a mají přístup i do internetu.

![statefull](screenshots/instances.png)
Instance běžící na různých hypervizorech

![statefull](screenshots/instance1.png)
První instance ping na druhou instanci.

![statefull](screenshots/instance2.png)
Druhá instance ping na první instanci.

![statefull](screenshots/network-topology.png)
Topologie sítí.

![statefull](screenshots/host-interface.png)
Nastavení ip na hostovi a jeho veth1 -> následně MASQUERADE pomocí iptables.
