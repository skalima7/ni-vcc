![statefull](screenshots/instance_xml_descriptor.png)
Xml virtuálky pomocí `virsh dumpxml instance-00000001`.

![statefull](screenshots/tcp_dump_instance_dhcp.png)
TCP Dump mezi virtuálkou a DHCP serverem.

![statefull](screenshots/wireshark_vxlan.png)
Trasování VXLAN pomocí nástroje Wireshark.

![statefull](east-west.jpg)
Diagram síťových prvků mezi dvěma virtuálkami.

> Na trasování metadat až k nova-API jsem si netroufl - nevím moc jak na to.

### Namátkové screenshoty z hledání názvů síťových prvků

![statefull](screenshots/master_br-tun.png)
Br-tun router.

![statefull](screenshots/master_br-int.png)
Br-int router.

![statefull](screenshots/router_ip_link.png)
Router `ip link` pomocí namespaces.

![statefull](screenshots/master_ip_link.png)
Master `ip addr`.
