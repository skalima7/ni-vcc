# k8s

Abych získal hlubší znalosti, tak jsem se rozhodl pro ruční nasazení pomocí nástroje `kubeadm`.

## Nasazení

Vycházel jsem z tohoto [návodu](https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/). Shrnutí postupu:

-   instalace kubelet kubeadm kubectl
-   instalace kontejner runtime - CRI-O
-   inicializace Master node
-   přidání Slave uzlů

![add_nodes_cmd](screenshots/cmd_add_nodes.png)

Ukázka přikazů pro přidání uzlů.

![get_nodes](screenshots/get_nodes.png)

Zobrazení jednotlivých uzlů v clusteru.

Následně pro získání UI rozhraní jsem nasadil Kubernetes Dashboard přes `kubectl` a yml definici.

![dashboard](screenshots/dashboard.png)

Ukázka Kubernetes Dashboard.

## Deploy

Pro deploy do k8s jsem využil Terraform - začínáme ho používat v práci, tak mám alespoň možnost si ho vyzkoušet.

![variables](screenshots/variables.png)

Ukázka nastavení proměnných pro terraform.

![deploy](screenshots/terraform-deployment.png)

Ukázka deployment definice pro terraform.

![pods](screenshots/pods.png)

Ukázka nasazených podů.

![pods](screenshots/deployments.png)

Ukázka vytvořeného deployment.
